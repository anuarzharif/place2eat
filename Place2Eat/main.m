//
//  main.m
//  Place2Eat
//
//  Created by mohd zharif bin anuar on 12/12/2016.
//  Copyright © 2016 Mohd Zharif Anuar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
