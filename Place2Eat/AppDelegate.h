//
//  AppDelegate.h
//  Place2Eat
//
//  Created by mohd zharif bin anuar on 12/12/2016.
//  Copyright © 2016 Mohd Zharif Anuar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

