//
//  LandingPage.m
//  Place2Eat
//
//  Created by mohd zharif bin anuar on 12/12/2016.
//  Copyright © 2016 Mohd Zharif Anuar. All rights reserved.
//

#import "LandingPage.h"

@interface LandingPage ()

@end

@implementation LandingPage

-(void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    float low_bound = 0;
    float high_bound = 47;
    float rndValue = (((float)arc4random()/0x100000000)*(high_bound-low_bound)+low_bound);
    
    NSLog(@"\n\ntesting sahaja %.0f\n\n", rndValue);
    
    [self setVariable];
    [self setGraphic];
}

-(void)setVariable
{
    
}

-(void)setGraphic
{
    self.title = @"Your Location";
}

-(IBAction)btnTapped:(id)sender
{
    int lowerBound = 0;
    int upperBound = 10;
    int x = lowerBound + arc4random_uniform(upperBound - lowerBound + 1);// arc4random() % 50;
    //    Generate random number between 500 and 1000:
    
    //    int x = (arc4random() % 501) + 500;
    
    //    NSInteger rand6 = [[GKRandomSource sharedRandom] nextIntWithUpperBound:6];
    //    static inline CGFloat frandom_range(CGFloat low, CGFloat high){ return (high-low)*frandom()+low;}
    //    long floating = (50 - 0) * random() + 0;
    
    NSLog(@"\n\ntesting sahaja %d\n\n", x);
}

-(void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
